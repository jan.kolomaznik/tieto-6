[remark]:<class>(center, middle)
# Kuchařka designu a vytváření kódu  

[remark]:<slide>(new)
## DRW, DRY a další principy

**DRW**: Don’t reinvent the wheel
- Nejjednodušší kód, který se má vyvinout, je kód, který vyvinul někdo jiný
- Nejlepší nápady jsou často „hrdě nalezeny jinde“, zatímco „nevymyslené“ je často restriktivní a škodlivé

[remark]:<slide>(wait)
**DRY**: Don’t repeat yourself
- Opětovné použití je jedním z cílů dobrého (objektově orientovaného) designu → usilovat o opětovné použití, a to i na úrovni kódu
- Když něco podobného vyřešíte podruhé, zeptejte se sami sebe, zda byste mohli učinit dřívější řešení všeobecnějším

[remark]:<slide>(wait)
Ale i ne-objevovat a znovu používat má nějakou cenu a neměly ty tyto pravidla stát nad vším.

[remark]:<slide>(new)
### Základní rady
Použijte zdravý rozum a své odborné znalosti
- I Pravidla mají výjimky, důležité je vědět, že je porušujete a za jakou cenu.
- Žádný nástroj nemůže učinit nic spolehlivého, nástroje neznají vaše záměry.
- **Od doporučení se můžete odchýlit, ale měli byste na to mít velmi dobrý důvod.**

[remark]:<slide>(wait)
Nepište kód pro sebe ale pro vašeho psychopatického kolegy, který ví, kde bydlíte.

[remark]:<slide>(wait)
Napište si jednoduchý test ve ktém kód použujete znovu.
Tak si sndno ověříte, že kód je znovupoužítelný.

[remark]:<slide>(wait)
Neprogogramujte sami, vždycky by kód měl po Vás někdo přečít.

[remark]:<slide>(new)
### Smysluplná jména
Jméno by mělo odpovědět na všechny základní otázky 
- Jaký je význam proměnné?
- Proč ta existuje?
- Co dělá?
- Jak se používá?
- **Mělo by usnadnit pochopení kódu!**

[remark]:<slide>(new)
### Jak vytvořit dobré jméno
Vyhnout se jménům typu `a1`,`l`,`O`

[remark]:<slide>(wait)
Smysluplné dobře vyhledatelné 

[remark]:<slide>(wait)
Jméno by mělo být rozmně dlouhé

[remark]:<slide>(wait)
Nepoužívejte podobná jména blízko sebe

[remark]:<slide>(wait)
Dělejte smysluplné rozdíly:
- `getActiveAccount()`
- `getActiveAccounts()`
- `getActiveAccountsInfo()`

[remark]:<slide>(wait)
Jak vytvořit dobré jméno

[remark]:<slide>(wait)
Dobré jméno se dá vyslovit!

[remark]:<slide>(wait)
Nedávejte do jména formát: `daydmY`

[remark]:<slide>(wait)
Nedávejte do jména typ: `private PhoneNumber phoneString`

[remark]:<slide>(new)
Členské předpony a další dekorace (`INeco`, `ANeco`, `m_`, ...)

[remark]:<slide>(wait)
Jedno slovo pro jeden pojem (z problémové domény, slovník)

[remark]:<slide>(wait)
#### Problém s jmény
Jména se mohou ztratit!
- Máte-li typy, použijte je.
- Problém jaký datový typ zvolit?

*Příklad*:
Navrhněte třídu osoba, u které evidujete 

- Jméno (celé i s tituly), 
- věk, 
- pohlaví 
- id.

[remark]:<slide>(new)
## Píšeme metodu: od jména k příkazům
Každá metoda by měla mít jednoduchou odpovědnost

[remark]:<slide>(wait)
#### „Udělejte tohle nebo tohle“:
- metoda je přetížena povinnostmi
- dělá několik různých věcí
- obvykle v závislosti na argumentech)

[remark]:<slide>(wait)
#### „Udělejte toto a toto a toto“: 
- metoda provádí řadu atomových akcí
- někdy přijatelných, např. pro fasády nebo šablony

[remark]:<slide>(wait)
#### Každá metoda by měla mít odpovídající název
- Pomáhá porozumět funkci metody
- Popisuje účel metody dobře - dobré a stručné názvy obvykle přicházejí snadno s přesně definovanými povinnostmi


[remark]:<slide>(new)
### Tělo metody
V rámci funkce se pohybujte vždy na jedné úrovně abstrakce.
- Dobře se odděluje aplikační logika od obslužných tříd.
- Jen jedna úroveň zanoření.

[remark]:<slide>(wait)
#### Více, ale jednodušších metod namísto méně všemocných
- Snadnější porozumět, používat, rozvíjet a udržovat
- Snižuje počet parametrů

[remark]:<slide>(wait)
#### Ortogonální metody
- Zodpovědnost metod se nepřekrývá, ortogonální metody nemohou být vyjádřeny jako posloupnost jiných metod
- Každá metoda má pak určitou „váhu“ - snadnější na pochopení a použití, méně na rozvoj a udržení

[remark]:<slide>(wait) 
#### Vyhněte se vnitřnímu členění metod
- Vždycky když máte chuť napsat komentář a teď dělám toto, vytvořte raději metodu.

[remark]:<slide>(new)
### Modifikátory viditelnosti
Vhodná viditelnost (nejméně možná)

Role s ohledem na dědictví = **Komunikační prostředek**

[remark]:<slide>(wait)
|               | (nic) | abstract | final |
| ---           |:---:|:---:|:---:|
| **private**   | (Pomocná metoda třídy) | ~ | ~ |
| **(defaut)**  | (Pomocný třída v balíčku) | ~ | ~ |
| **protected** | Notifikace o stavu | Doplnění kódu | ~ |
| **public**    | Volné použití | ~ | Použij, ale neměn |

##### Notes
Nepoužívejte switch! 
- Raději použijte polymorfismus

[remark]:<slide>(new)
## Overloading, overriding
Nezaměňujte přetížení a překrytí
- Ale i když znáte rozdíl, uživatelé vašeho kódu nemusí - zkuste si je zaměňovat

[remark]:<slide>(wait)
#### Tři základní pravidla:
1. Při překrytí vždy použijte annotaci `@Override`!
2. Přetížené metody by měly dělat (v podstatě) totéž.
3. Přepsání je běžné, přetížení je výjimkou (s výjimkou konstruktorů)

[remark]:<slide>(wait)
#### Přetížení může být matoucí s dědictvím
- Přidání metody přetížení do zděděného typu je pro mnoho lidí obzvláště matoucí
- Přetěžování nehody… může také nastat
- Při přepsání přetížené metody je snadné na něco zapomenout (např. Přepsat ostatní metody)

[remark]:<slide>(new)
## Zpracování parametrů
Pečlivě vyberte typy parametrů
- Vyžadovat co nejméně - preferujte více abstraktních typů, pokud je to možné
- Použijte více popisných typů:
  - `enum` nad booleovské hodnoty
  - vyhněte se `Map` a `Properties`
  - pro flagy použít `EnumSet`
  
[remark]:<slide>(wait)
#### Omezte sadu parametrů
Sada parametrů by měla sledovat logický účel metody, její abstrakce
- Ideální počet argumentů funkce je: **0**

Jeden argument:
- Dotaz a transformace
- Událost

Dva a tři argumenty …

[remark]:<slide>(new)
#### Vyhněte se:
- dlouhé seznamy parametrů
- návratové parametry (jedinná výjimka `Writer/OutputStream`)
- logickým argumentů, obzvláště když se jedná o jediný.
- Nepoužívejte výstupní argumenty.
- Používejte návrhový vzor přepravka.
- Více parametrů stejného typu za sebou (`assertEquels(?, ?)`)

[remark]:<slide>(new)
## Používání varargs
Podporován poli se všemi jejich negativními stranami

Problémy s generiky (anotace `@SafeVarargs` pomáhá trochu)

Bezpečnostní otázky vyžadující obranný přístup (viz později)

Není tak snadné implementovat správně, když požadujete alespoň některé parametry

Přetížení může vše dohromady, ale často nutné

Důvody účinnosti (přetížení pro typické počty parametrů)

Je třeba podporovat minimální povinný počet parametrů

Varargs používejte uvážlivě: pouze tehdy, když to dává smysl a když metoda skutečně pracuje na sekvencích hodnot s proměnnou délkou


[remark]:<slide>(new)
## Hodnota `null`

*“I call it my billion-dollar mistake. 
It was the invention of the null reference in 1965. 
At that time, I was designing the first comprehensive type system for references in an object oriented language. 
My goal was to ensure that all use of references should be absolutely safe, with checking performed automatically by the compiler. 
But I couldn't resist the temptation to put in a null reference […]”*
**Tony Hoare**

[remark]:<slide>(wait)
Vyhnout se nulovým odkazům je obecně dobrý nápad.

Snažte se jej zakázat a nahradit alternativami

Použijte kontrolní nástroje, fanatici mohou používat anotace

Tento přístup jednoznačně dokumentujte

[remark]:<slide>(new)
### Nahrazení `null` speciálními instancemi
Prázdné kontejnery (například `Collections.empty*()`)

[remark]:<slide>(wait)
Výchozí instance (Null Objekty)

```java
public class Point {
    
    public static final Point UNDEFINE = new Point(Double.NaN, Double.NaN) {
    
        public double distanceTo(Point other) {
                return Double.NaN;
            }
    };
    
    private double x, y;
    
    // Constructor
    
    public double distanceTo(Point other) {
        double dx = this.x - other.y;
        double dy = this.y - other.y;
        return Math.sqrt((dx * dx) + (dy * dy));
    }
}
```

[remark]:<slide>(new)
### Použití objektu `Optional<T>`
Prodramátor jasně vidí, že hodnota, se kterou pracuje nemusí existovat.

Použití:
```java
Optional<String> hello = Optional.of("Hello world");
Optional<String> empty = Optional.empty();
```

[remark]:<slide>(wait)
Option má pomocné metody:
- `get()`: Pro získání hodnoty
- `isEmpty()` a `isPresent()`: Testují přítomnost hodnoty
- `orElse​(T other)`: Vrátí uloženou hodnotu nebo výchozí
- `orElseThrow()`: Vrátí uloženou hodnotu nebo vyhodí výjimku `NoSuchElementException`
- `orElseThrow(Supplier<? extends X> exceptionSupplier)`: Vrátí uloženou hodnotu nebo vyhodí definovanou výjimku
- `orElseGet​(Supplier<? extends T> supplier)`: Pro získání hodnoty nebo provolání kódu který hodnotu vygeneruje.
- `map​(Function<? super T,​? extends U> mapper)`: Pokud operioan neni empty, transformuje jej na jiný.
- `filter​(Predicate<? super T> predicate)`: Vrátí prázný Opetional, pokud hodnota neodpovídá podmínce.

[remark]:<slide>(new)
### Využití více statických kontrol
Jedná se o spíše o *hašení požáru* ne jeho předcházení.

- Možno použít statický kód (`if (? == null) throw new Illegal...`)
- Možné jsou taky annotacepro kontrolu, ale ...
- Projekt Lombok a annotaci [`@NonNull`](https://projectlombok.org/features/NonNull)

Není povinné v Javě, proto nevyřeší problém

Nutno správně konfigurovat validační frameworky

Řešení založená na `assert` slouží pro testování, ale ne pro nasazení.

[remark]:<slide>(new)
## Styly psaní kódu, idiomy a vzory
Vyvážený postup shora dolů a zdola nahoru
- Myšlení dopředu poskytuje směr shora dolů
- Ověření a testování zajišťuje směr zdola nahoru


*Zanechte tábořiště čistší, než jste jej nalezli.*
**Skautské pravidlo :)**

[remark]:<slide>(wait)
#### Udělejte kód tak, abyste ho mohli „na první pohled chytit“
- Udržujte metody přiměřeně dlouhé
- Měly by se hodit na jednu nebo dvě obrazovky
- Refactor delší kód se soukromými pomocníky

[remark]:<slide>(wait)
#### Udržujte své metody strukturované
- Oddělte logické bloky s prázdnými řádky a komentáři
- Udělejte typickou cestu kódu rovnou a čistou
- Zvažte extraxci složitých podmínek do metody

[remark]:<slide>(new)
#### Udržujte své třídy organizované
- Nejprve deklarujte konstanty,
- po nich atributy třídy
- pak statické metody,
- pak by měly následova konstruktory
- `public` a `protected` metody
- pomocné souktormé metody

[remark]:<slide>(wait)
### Co naopak nedělat
Nedávejte příliš mnoho na jeden řádek
- Stohovací stopy mohou přesně určit tvrzení
- Užitečné také pro ladění

Vyhněte se záhadnému nebo příliš hustému kódu

Vyhněte se hlubokému zanoření (rozsahy, podmínky, smyčky, dokonce třídy atd.)


[remark]:<slide>(new)
### Komentář kódu
**Špatný kód nespraví dobrý komentář**
- „Kód by měl být samo dokumentující“
- „Zákaz všech komentářů je hloupý“
- Komentovat **proč** nikdy ne *jak*. 

[remark]:<slide>(wait)
#### Společné pravidlo pro pravidelné komentáře:
- Vysvětlit nejasné struktury: *(regulární výrazy)*
- Vysvětlit záměr
- Varovat
- Zvýraznit kritické části kódu 
- TODO komentáře

[remark]:<slide>(new)
### Práce s parametry
Proč kontrolovat argumenty?
- Porušení precodition ohrožuje běh programu,
- může mít za následek závažné chyby v kódu.

[remark]:<slide>(wait)
#### „Fail fast“: co nejdříve zkontrolujte argumenty
- Včasné kontroly jsou způsob, jak dosáhnout atomové odolnosti
- Skutečný kód zpracování se může zaměřit na vlastní práci

[remark]:<slide>(wait)
#### Pokyny a techniky
Zveřejněná metoda musí počítt s výjimkami
- Soukromé metody (a někdy i třídy) nemusejí, 
- obvykle ani není žádoucí (např. Z důvodů výkonu)
 
Používejte standardní výjimky, pokud je to možné, dodržujte konvence

Použijte znovu použitelné kontrolní metody

Používejte techniky obranného kopírování

[remark]:<slide>(new)
### Defenzivní kopírování
Argumentem může být trojský kůň

Vynucení vaší metody, aby udělala něco špatného, ​​i když zkontrolujete argumenty.

[remark]:<slide>(wait)
#### Vytvářejte obranné kopie proměnných argumentů
- Pro kopie použijte důvěryhodné implementace
- Zkontrolujte kopie místo originálů

[remark]:<slide>(wait)
#### Důvody pro použití důvěryhodné implementace
- Rozšiřitelné třídy
- Použití klonu ()
- Falešný neměnný

Další bod ve prospěch neměnných finálních tříd

**Defenzivni kopie jsou hnodné i pro návratové typy.**

[remark]:<slide>(new)
### Lokální proměnné
Minimalizujte rozsah lokálních proměnných
- Díky tomu je kód čitelnější
- Snižuje pravděpodobnost chyby (skrytí jména, záměna rozsahu)
- To by mohlo kompilátoru pomoci optimalizovat
- V případě potřeby deklarovat proměnné
- Udržujte metody malé a soustředěné

9Při deklaraci inicializujte proměnné
- Je to signál pro čtenáře, pro co proměnná slouží
- Problémy v některých trukturách (`try`)


[remark]:<slide>(wait)
#### Snažte se používat `final` proměnné
- Zabraňuje nechtěnému přepsání hodnoty
- Některé konstrukty dokonce vyžadují
- Pomáháte kompilátoru při optimalizaci
- Java umi detekovat **efective final** proměnné

[remark]:<slide>(new)
### Použití rozhraní
Vždy se snažte použít to nejabstraktnější rozhraní

Tím říkáte co skutečně v kódu potřebujete

[remark]:<slide>(wait)
#### Preferujte rozhraní před reflexí
- Reflexe je křehká, postrádá kompilační kontroly
- Kód je podrobný a neohrabaný
- Výkon hodně trpí
- Použitelné ještě pro rámce poskytovatele (s továrním vzorem)

[remark]:<slide>(wait)
#### Preferujte lambda pro anonymní třídy
- Účinnější a kompaktnější
- Velmi vzácné případy, kdy je tato anonymní třída potřebná

[remark]:<slide>(new)
### Iterační vzory
Upřednostňujte je před `while`
- Umožňuje deklarovat proměnnou smyčky → zmenšení rozsahu proměnné
- Může být kratší, tj. Čitelnější
- To by mohlo kompilátoru pomoci optimalizovat
- Příklad (stále) správného idiomu smyčky:

```java
for (Iterator<T> it = c.iterator(); it.hasNext();) {
    // TODO
}
```

[remark]:<slide>(wait)
#### Použávjte `for-each` smyčku
- Umožňuje deklarovat proměnnou smyčky (pouze jednu)
- Ještě kratší, čitelnější a méně náchylný k chybám
- To by mohlo kompilátoru pomoci optimalizovat
- Není použitelné vždy (filtrování, transformace, paralelní nebo nepravidelná iterace)

[remark]:<slide>(new)
#### Pokud je to možně používejte `steams`
- Výraznější a kompaktnější
- Podporuje lepší složení a opakované použití kódu
- Snadné paralelní zpracování

[remark]:<slide>(wait)
### Řetězce: `String`
Řetězy jsou špatné náhražky jiných typů

Pokud používáte parametry v rámci (např. `StringBuilder`, `String.format`), zvažte lépe čitelné alternativy

Vyhněte se `StringBuffer`, pokud to nevyžaduje rozhraní


[remark]:<slide>(new)
## Testování, ladění, optimalizace
Testování je první velkou výzvou jak pro váš kód, tak pro jeho design

[remark]:<slide>(wait)
#### Testovací kód ukazuje, jak používat rozhraní
- Samotné napsání testu je test návrhu rozhraní
- Závěrečné testy slouží jako dokumentace k použití kódu

[remark]:<slide>(wait)
#### Testy také ověřují kvalitu návrhu
- Zvláště zajímavé jsou testy krajních případú
- Testovací kód odhaluje závislosti

[remark]:<slide>(new)
### Debuggig nástroj: `assert`
Dobré pro všechny přísně vnitřní kontroly
- Kontroly parametrů v soukromých metodách
- Kontroly před a po stavbě, invariantní kontroly

[remark]:<slide>(wait)
#### Dokumentační význam
Podmínky a invarianty jsou vyjádřeny jako hodnotitelné booleovské výrazy

[remark]:<slide>(wait)
#### Podpora ladění
- Podpora testování v "white box"
- `asserts` mohou zůstat v produkčním kódu
- mohou být jednoduše zapnuta / vypnuta
- testovací prostředí může běžet s povolenými tvrzeními po dlouhou dobu,

**`assert` není určeno pro kontroly parametrů metod.**

[remark]:<slide>(new)
### Loggování
Používejte konzistentně úrovně protokolu, například:
- **Error** a** warning**: ...
- **Info** pro zprávy, které by měl číst správe systému
- **Debug** pro podrobnosti pro vývojaře
- **Trace** podrobností pro ladění kódu

[remark]:<slide>(wait)
#### Vždy předejte dostatek diagnostických dat
- Vždy projděte související výjimky
- Poskytovat bohaté a podrobné zprávy
- Zvažte prokládání událostí (poskytněte kontext ke sledování toku)

[remark]:<slide>(wait)
#### Zvažte výkon
- Použijte podmíněné logování nebo inteligentní loggery
- Vyhněte se logování pod zámky a v časově kritických místech

[remark]:<slide>(wait)
#### Vyhněte se logování výjimky více než jednou

[remark]:<slide>(new)
### Optimalizace: principy

*„Premature optimization is the root of all evil“*
**Donald E. Knuth**

Záladní pravdlo optimalizace je **neoptimalizovat**!

[remark]:<slide>(wait)
#### Měřit, měřit a ještě jednou měřit
- Profilování by mělo předcházet jakýmkoliv pokusům o optimalizaci
- Změřte před změnou (po jejím zanesení)

[remark]:<slide>(wait)
#### Snažte se spíše o dobré programy než o rychlé
- Obecně platí, že dobré API je v souladu s dobrým výkonem
- Neprohýbejte rozhraní API, abyste dosáhli dobrého výkonu, vyhněte se konstrukcím motivovaným výkonem
- Vyhněte se návrhům rozhodnutí, která omezují výkon, ale při navrhování rozhraní API zvažte výkon

[remark]:<slide>(new)
### Životní cyklus objektu
Pokud to není nutné, nevytvářejte nové objekty
- Preferovat sdílení objektů (vyžaduje, aby objekty byly neměnné)
- Vyhněte se boxing a polím primitivního typu (včetně varargs)
- Vyvarujte se vytváření nových objektů za každou cenu

[remark]:<slide>(wait)
#### Nezapomeňte uvolnovat reference
- Memory-leak je možný i s garbage collector
- Zvláště složité je použití thread-lokal proměnných

[remark]:<slide>(wait)
#### Pochopte, jak funguje garbage collector
- Kolekce založená na generaci podporuje některé modely; Vytváření objektů s krátkou životností je způsobem Java
- Nezapomeňte, že se garbage collector vyvíjí

[remark]:<slide>(new)
### Nízkoúrovňová optimalizace
O JVM to příliš nepředpokládejte
- Různé implementace a platforma se mohou chovat odlišně
- JVM se vyvíjí, zlepšuje ponechte nízkoúrovňovou optimalizaci pro JVM

[remark]:<slide>(wait)
#### Vyhněte se nativnímu rozhraní Java
- Problémy s přenositelností → lépe se tomu vyhnout, pokud je to možné
- Důvody výkonu jsou zřídka platné
- Implementace JVM se rychle zvyšuje, dobrý hotspot optimalizátor dokáže lépe než průměrný nativní překladač

#### Vyhněte se reflexi
- Reflexe ruší optimalizaci JVM
















