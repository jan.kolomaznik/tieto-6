package jm.dp.nullObject;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Client {

    public List<String> task() {
        // logic to get list of tasks
        // if there are any
        if (!"any".isEmpty()) {
            return Arrays.asList("A", "B", "A");
        } else {
            return Collections.emptyList();
        }
    }

    public Point getCurrentLocation() {
        if (!"i can".isEmpty()) {
            return new Point(1,2);
        } else {
            return Point.NULL;
        }
    }
}
