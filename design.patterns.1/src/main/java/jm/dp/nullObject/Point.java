package jm.dp.nullObject;

public class Point {

    public static final Point NULL = new Point(0,0) {

        @Override
        public double distance(Point o) {
            return Double.POSITIVE_INFINITY;
        }
    };

    private final int x;

    private final int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public double distance(Point o) {
        if (NULL == o) {
            return o.distance(this);
        }
        return Math.sqrt(Math.pow(x - o.x, 2)+Math.pow(y - o.y, 2));
    }
}
