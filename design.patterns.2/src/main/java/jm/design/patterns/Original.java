package jm.design.patterns;

import java.util.HashMap;
import java.util.Map;

public class Original {

    public static void main(String[] args) {
        // Example String as original
        String one = String.valueOf("Ahoj");
        String two = new String("Ahoj");
        System.out.format("(%s == %s) = %s %n", one, two, (one == two));

        System.out.format("red: %s %n", Color.color("#FF0000"));
        System.out.format("red: %s %n", Color.color("#FF0000"));
    }

    public static class Color {

        private static Map<String, Color> colorMap = new HashMap<>();

        public static Color color(String value) {
            if (colorMap.containsKey(value)) {
                return colorMap.get(value);
            }

            Color color = new Color(value);
            colorMap.put(value, color);
            return color;
        }

        private final String value;

        private Color(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "Color: {" + super.toString().substring(super.toString().indexOf("@")) +
                    ", value= " + value + "}";
        }
    }
}
