package jm.design.patterns;

public interface ActionCreator {

    String execute();
}
