package jm.design.patterns;

import java.util.LinkedList;
import java.util.Queue;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;

public class Command {

    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        calculator.doPlus(1,2);
        calculator.doMinus(3,2);
        calculator.undo();
        calculator.undo();
    }

    // Cizi, nechci menit
    public static class Math {

        public void plus(double a, double b) {
            System.out.format("%f + %f = %f\n", a, b, a + b);
        }

        public void minus(double a, double b) {
            System.out.format("%f - %f = %f\n", a, b, a - b);
        }
    }

    public static class Calculator {

        private Math math = new Math();
        private Queue<Runnable> undo = new LinkedList<>();

        public void doPlus(double a, double b) {
            Plus plus = new Plus(math, a, b);
            plus.run();
            undo.add(plus);
        }

        public void doMinus(double a, double b) {
            Minus minus = new Minus(math, a, b);
            minus.run();
            undo.add(minus);
        }

        public void undo() {
            Runnable todo = undo.remove();
            if (todo != null) {
                todo.run();
            }
        }
    }

    public static class Plus implements Runnable {

        private final Math math;
        private final double a, b;

        public Plus(Math math, double a, double b) {
            this.math = math;
            this.a = a;
            this.b = b;
        }

        @Override
        public void run() {
            math.plus(a, b);
        }
    }

    public static class Minus implements Runnable {

        private final Math math;
        private final double a, b;

        public Minus(Math math, double a, double b) {
            this.math = math;
            this.a = a;
            this.b = b;
        }

        @Override
        public void run() {
            math.minus(a, b);
        }
    }
}
