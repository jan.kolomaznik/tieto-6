[remark]:<class>(center, middle)
# Přehlednávrhových vzorů:  
----
*Tvorba složitých objektů*

[remark]:<slide>(new)
## Factory Method
Deklaruje rozhraní s metodou pro získání objektu. 

Rozhodnutí o konkrétním typu vráceného objektu však ponechává na svých potomcích, tj. na překrytých verzích deklarované metody.

[remark]:<slide>(wait)
![](media/Factory_Method.svg)

[remark]:<slide>(new)
### Příklad implementace
Create an interface.

```java
public interface Shape {
   void draw();
}
```

[remark]:<slide>(wait)
Create concrete classes implementing the same interface.

```java
public class Rectangle implements Shape {
   
    @Override
   public void draw() { System.out.println("Inside Rectangle::draw method."); }
}
```

```java
public class Square implements Shape {
   
    @Override
   public void draw() { System.out.println("Inside Square::draw method."); }
}
```

```java
public class Circle implements Shape {
    
   @Override
   public void draw() { System.out.println("Inside Circle::draw() method."); }
}
```

[remark]:<slide>(new)
Create a Factory to generate object of concrete class based on given information.

```java
public class ShapeFactory {
	
   //use getShape method to get object of type shape 
   public Shape getShape(String shapeType){
      if(shapeType == null){
         return null;
      }		
      if(shapeType.equalsIgnoreCase("CIRCLE")){
         return new Circle();
         
      } else if(shapeType.equalsIgnoreCase("RECTANGLE")){
         return new Rectangle();
         
      } else if(shapeType.equalsIgnoreCase("SQUARE")){
         return new Square();
      }
      
      return null;
   }
}
```

[remark]:<slide>(new)
Use the Factory to get object of concrete class by passing an information such as type.

```java
public class FactoryPatternDemo {

   public static void main(String[] args) {
      ShapeFactory shapeFactory = new ShapeFactory();

      //get an object of Circle and call its draw method.
      Shape shape1 = shapeFactory.getShape("CIRCLE");

      //call draw method of Circle
      shape1.draw();

      //get an object of Rectangle and call its draw method.
      Shape shape2 = shapeFactory.getShape("RECTANGLE");

      //call draw method of Rectangle
      shape2.draw();

      //get an object of Square and call its draw method.
      Shape shape3 = shapeFactory.getShape("SQUARE");

      //call draw method of circle
      shape3.draw();
   }
}
```

[remark]:<slide>(new)
## Prototype
Jak vytvořit kopii existujícího objektu místo vytváření nové třídy.

[remark]:<slide>(wait)  
![](media/Prototype.svg)

[remark]:<slide>(new)
### Příklad implementace
Create an abstract class implementing Clonable interface.

```java
public abstract class Shape implements Cloneable {
   
   private String id;
   protected String type;
   
   abstract void draw();
   
   public String getType(){
      return type;
   }
   
   public String getId() {
      return id;
   }
   
   public void setId(String id) {
      this.id = id;
   }
   
   public Object clone() {
      Object clone = null;
      
      try {
         clone = super.clone();
         
      } catch (CloneNotSupportedException e) {
         e.printStackTrace();
      }
      
      return clone;
   }
}
```

[remark]:<slide>(new)
Create concrete classes extending the above class.

```java
public class Rectangle extends Shape {

   public Rectangle(){
     type = "Rectangle";
   }

   @Override
   public void draw() {
      System.out.println("Inside Rectangle::draw method.");
   }
}
```

```java
public class Square extends Shape {

   public Square(){
     type = "Square";
   }

   @Override
   public void draw() {
      System.out.println("Inside Square::draw method.");
   }
}
```

```java
public class Circle extends Shape {

   public Circle(){
     type = "Circle";
   }

   @Override
   public void draw() {
      System.out.println("Inside Circle::draw method.");
   }
}
```

[remark]:<slide>(new)
Create a class to get concrete classes from database and store them in a Hashtable.

```java
import java.util.Hashtable;

public class ShapeCache {
	
   private static Hashtable<String, Shape> shapeMap  = new Hashtable<>();

   public static Shape getShape(String shapeId) {
      Shape cachedShape = shapeMap.get(shapeId);
      return (Shape) cachedShape.clone();
   }

   // for each shape run database query and create shape
   // shapeMap.put(shapeKey, shape);
   // for example, we are adding three shapes
   
   public static void loadCache() {
      Circle circle = new Circle();
      circle.setId("1");
      shapeMap.put(circle.getId(),circle);

      Square square = new Square();
      square.setId("2");
      shapeMap.put(square.getId(),square);

      Rectangle rectangle = new Rectangle();
      rectangle.setId("3");
      shapeMap.put(rectangle.getId(), rectangle);
   }
}
```

[remark]:<slide>(new)
PrototypePatternDemo uses ShapeCache class to get clones of shapes stored in a Hashtable.

```java
public class PrototypePatternDemo {
   public static void main(String[] args) {
      ShapeCache.loadCache();

      Shape clonedShape = (Shape) ShapeCache.getShape("1");
      System.out.println("Shape : " + clonedShape.getType());		

      Shape clonedShape2 = (Shape) ShapeCache.getShape("2");
      System.out.println("Shape : " + clonedShape2.getType());		

      Shape clonedShape3 = (Shape) ShapeCache.getShape("3");
      System.out.println("Shape : " + clonedShape3.getType());		
   }
}
```

[remark]:<slide>(new)
## Builder
Návrhový vzor řeší problém, jak oddělit vytváření složitých objektů od jejich prezentace, aby stejný proces konstrukce mohl mít za výsledek rozdílný způsob prezentace.

[remark]:<slide>(wait)
![](media/Builder.svg)

[remark]:<slide>(new)
### Příklad implementace
Create an interface Item representing food item and packing.

```java
public interface Item {
   public String name();
   public Packing packing();
   public float price();	
}
```

```java
public interface Packing {
   public String pack();
}
```

[remark]:<slide>(new)
Create concrete classes implementing the Packing interface.

```java
public class Wrapper implements Packing {

   @Override
   public String pack() {
      return "Wrapper";
   }
}
```

```java
public class Bottle implements Packing {

   @Override
   public String pack() {
      return "Bottle";
   }
}
```

[remark]:<slide>(new)
Create abstract classes implementing the item interface providing default functionalities.

```java
public abstract class Burger implements Item {

   @Override
   public Packing packing() {
      return new Wrapper();
   }

   @Override
   public abstract float price();
}
```

```java
public abstract class ColdDrink implements Item {

	@Override
	public Packing packing() {
       return new Bottle();
	}

	@Override
	public abstract float price();
}
```

[remark]:<slide>(new)
Create concrete classes extending Burger and ColdDrink classes

```java
public class VegBurger extends Burger {

   @Override
   public float price() {
      return 25.0f;
   }

   @Override
   public String name() {
      return "Veg Burger";
   }
}
```

```java
public class ChickenBurger extends Burger {

   @Override
   public float price() {
      return 50.5f;
   }

   @Override
   public String name() {
      return "Chicken Burger";
   }
}
```

```java
public class Coke extends ColdDrink {

   @Override
   public float price() {
      return 30.0f;
   }

   @Override
   public String name() {
      return "Coke";
   }
}
```

```java
public class Pepsi extends ColdDrink {

   @Override
   public float price() {
      return 35.0f;
   }

   @Override
   public String name() {
      return "Pepsi";
   }
}
```

[remark]:<slide>(new)
Create a Meal class having Item objects defined above.

```java
import java.util.ArrayList;
import java.util.List;

public class Meal {
   private List<Item> items = new ArrayList<Item>();	

   public void addItem(Item item){
      items.add(item);
   }

   public float getCost(){
      float cost = 0.0f;
      
      for (Item item : items) {
         cost += item.price();
      }		
      return cost;
   }

   public void showItems(){
   
      for (Item item : items) {
         System.out.print("Item : " + item.name());
         System.out.print(", Packing : " + item.packing().pack());
         System.out.println(", Price : " + item.price());
      }		
   }	
}
```

[remark]:<slide>(new)
Create a MealBuilder class, the actual builder class responsible to create Meal objects.

```java
public class MealBuilder {

   public Meal prepareVegMeal (){
      Meal meal = new Meal();
      meal.addItem(new VegBurger());
      meal.addItem(new Coke());
      return meal;
   }   

   public Meal prepareNonVegMeal (){
      Meal meal = new Meal();
      meal.addItem(new ChickenBurger());
      meal.addItem(new Pepsi());
      return meal;
   }
}
```

[remark]:<slide>(new)
BuiderPatternDemo uses MealBuider to demonstrate builder pattern.

```java
public class BuilderPatternDemo {
   public static void main(String[] args) {
   
      MealBuilder mealBuilder = new MealBuilder();

      Meal vegMeal = mealBuilder.prepareVegMeal();
      System.out.println("Veg Meal");
      vegMeal.showItems();
      System.out.println("Total Cost: " + vegMeal.getCost());

      Meal nonVegMeal = mealBuilder.prepareNonVegMeal();
      System.out.println("\n\nNon-Veg Meal");
      nonVegMeal.showItems();
      System.out.println("Total Cost: " + nonVegMeal.getCost());
   }
}
```

[remark]:<slide>(new)
## Abstract Factory
Abstract Factory Pattern řeší problém, jak vytvořit na základě rozhodnutí v běhu programu instanci třídy, která dále vytváří instanci souvisejících nebo závislých tříd.

![](media/Abstract_Factory.svg)
