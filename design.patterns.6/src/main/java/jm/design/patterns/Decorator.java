package jm.design.patterns;

public class Decorator {

    public static void main(String[] args) {
        Shape circle = new Circle();
        Shape redCircle = new RedShapeDecorator(new Circle());
        Shape blueFillCicle = new FillBlueShapeDecorator(redCircle);
        Shape redRectangle = new RedShapeDecorator(new Rectangle());
        System.out.println("Circle with normal border");
        circle.draw();
        System.out.println("\nCircle of red border");
        blueFillCicle.draw();
        System.out.println("\nRectangle of red border");
        redRectangle.draw();
    }

    public interface Shape {
        void draw();
    }

    public static class Rectangle implements Shape {
        @Override
        public void draw() {
            System.out.println("Shape: Rectangle");
        }
    }

    public static class Circle implements Shape {
        @Override
        public void draw() {
            System.out.println("Shape: Circle");
        }
    }

    public static abstract class ShapeDecorator implements Shape {

        protected Shape decoratedShape;

        public ShapeDecorator(Shape decoratedShape){
            this.decoratedShape = decoratedShape;
        }

        public void draw(){
            decoratedShape.draw();
        }
    }

    public static class RedShapeDecorator extends ShapeDecorator {
        public RedShapeDecorator(Shape decoratedShape) {
            super(decoratedShape);
        }
        @Override
        public void draw() {
            decoratedShape.draw();
            setRedBorder(decoratedShape);
        }

        private void setRedBorder(Shape decoratedShape){
            System.out.println("Border Color: Red");
        }
    }

    public static class FillBlueShapeDecorator extends ShapeDecorator {
        public FillBlueShapeDecorator(Shape decoratedShape) {
            super(decoratedShape);
        }
        @Override
        public void draw() {
            decoratedShape.draw();
            fillColor(decoratedShape);
        }

        private void fillColor(Shape decoratedShape){
            System.out.println("Fill Color: blue");
        }
    }
}
