[remark]:<class>(center, middle)
# Přehlednávrhových vzorů:  
----
*Jak přizpůsobit svoje třídy jiným*

[remark]:<slide>(new)
## Bridge
Bridge Pattern představuje problém oddělení rozhraní třídy od její vlastní implementace, aby obě tyto části mohly být vytvářeny nezávisle na sobě. 

Tento princip zajistí, že může být změněna implementace třídy bez toho, abychom měnili kód klienta.

[remark]:<slide>(wait)
![](media/Bridge.svg)

[remark]:<slide>(new)
### Příklad implementace
Create bridge implementer interface.

```java
public interface DrawAPI {
   public void drawCircle(int radius, int x, int y);
}
```

[remark]:<slide>(wait)
Create concrete bridge implementer classes implementing the DrawAPI interface.

```java
public class RedCircle implements DrawAPI {
   @Override
   public void drawCircle(int radius, int x, int y) {
      System.out.println("Drawing Circle[ color: red, " +
                         "radius: " + radius + ", x: " + x + ", y: " + y + "]");
   }
}
```

```java
public class GreenCircle implements DrawAPI {
   @Override
   public void drawCircle(int radius, int x, int y) {
      System.out.println("Drawing Circle[ color: green, " +
                         "radius: " + radius + ", x: " + x + ", y: " + y + "]");
   }
}
```

[remark]:<slide>(new)
Create an abstract class Shape using the DrawAPI interface.

```java
public abstract class Shape {
   protected DrawAPI drawAPI;
   
   protected Shape(DrawAPI drawAPI){
      this.drawAPI = drawAPI;
   }
   public abstract void draw();	
}
```

[remark]:<slide>(wait)
Create concrete class implementing the Shape interface.

```java
public class Circle extends Shape {
   private int x, y, radius;

   public Circle(int x, int y, int radius, DrawAPI drawAPI) {
      super(drawAPI);
      this.x = x;  
      this.y = y;  
      this.radius = radius;
   }

   public void draw() {
      drawAPI.drawCircle(radius,x,y);
   }
}
```

[remark]:<slide>(new)
Use the Shape and DrawAPI classes to draw different colored circles.

```java
public class BridgePatternDemo {
   public static void main(String[] args) {
      Shape redCircle = new Circle(100,100, 10, new RedCircle());
      Shape greenCircle = new Circle(100,100, 10, new GreenCircle());

      redCircle.draw();
      greenCircle.draw();
   }
}
```

[remark]:<slide>(new)
## Strategy
Definuje množinu vyměnitelných objektů (algoritmů) řešící zadanou úlohu a umožňuje mezi nimi dynamicky přepínat.

[remark]:<slide>(wait)  
![](media/Strategy.svg)  

[remark]:<slide>(new)
### Příklad strategy
![](media/Strategy_example.svg)  

[remark]:<slide>(new)
### Příklad implementace
Create an interface.

```java
public interface Strategy {
   public int doOperation(int num1, int num2);
}
```

[remark]:<slide>(new)
Create concrete classes implementing the same interface.

```java
public class OperationAdd implements Strategy{
   @Override
   public int doOperation(int num1, int num2) {
      return num1 + num2;
   }
}
```

```java
public class OperationSubstract implements Strategy{
   @Override
   public int doOperation(int num1, int num2) {
      return num1 - num2;
   }
}
```

```java
public class OperationMultiply implements Strategy{
   @Override
   public int doOperation(int num1, int num2) {
      return num1 * num2;
   }
}
```

[remark]:<slide>(new)
Create Context Class.

```java
public class Context {
   private Strategy strategy;

   public Context(Strategy strategy){
      this.strategy = strategy;
   }

   public int executeStrategy(int num1, int num2){
      return strategy.doOperation(num1, num2);
   }
}
```

[remark]:<slide>(new)
Use the Context to see change in behaviour when it changes its Strategy.

```java
public class StrategyPatternDemo {
   public static void main(String[] args) {
      Context context = new Context(new OperationAdd());		
      System.out.println("10 + 5 = " + context.executeStrategy(10, 5));

      context = new Context(new OperationSubstract());		
      System.out.println("10 - 5 = " + context.executeStrategy(10, 5));

      context = new Context(new OperationMultiply());		
      System.out.println("10 * 5 = " + context.executeStrategy(10, 5));
   }
}
```

[remark]:<slide>(new)
## Model-View-Controler
Nejbežnější návrhový vzor pro práci s uživatelským rozhraním.

Existuje množství odvozených vzorů, defakto co UI knihovna to jiná implementace.

Odděluje části přebírající požadavky od užitvatele od zbytku aplikace a částí zabývajícéch se zobrazením.

[remark]:<slide>(wait)
![](media/MVC.png)

[remark]:<slide>(new)
### Příklad implementace
Create Model.

```java
public class Student {
   private String rollNo;
   private String name;
   
   public String getRollNo() {
      return rollNo;
   }
   
   public void setRollNo(String rollNo) {
      this.rollNo = rollNo;
   }
   
   public String getName() {
      return name;
   }
   
   public void setName(String name) {
      this.name = name;
   }
}
```

[remark]:<slide>(new)
Create View.

```java
public class StudentView {
   public void printStudentDetails(String studentName, String studentRollNo){
      System.out.println("Student: ");
      System.out.println("Name: " + studentName);
      System.out.println("Roll No: " + studentRollNo);
   }
}
```

[remark]:<slide>(new)
Create Controller.

```java
public class StudentController {
   private Student model;
   private StudentView view;

   public StudentController(Student model, StudentView view){
      this.model = model;
      this.view = view;
   }

   public void setStudentName(String name){
      model.setName(name);		
   }

   public String getStudentName(){
      return model.getName();		
   }

   public void setStudentRollNo(String rollNo){
      model.setRollNo(rollNo);		
   }

   public String getStudentRollNo(){
      return model.getRollNo();		
   }

   public void updateView(){				
      view.printStudentDetails(model.getName(), model.getRollNo());
   }	
}
```

[remark]:<slide>(new)
Use the StudentController methods to demonstrate MVC design pattern usage.

```java
public class MVCPatternDemo {
   public static void main(String[] args) {

      //fetch student record based on his roll no from the database
      Student model  = retriveStudentFromDatabase();

      //Create a view : to write student details on console
      StudentView view = new StudentView();

      StudentController controller = new StudentController(model, view);

      controller.updateView();

      //update model data
      controller.setStudentName("John");

      controller.updateView();
   }

   private static Student retriveStudentFromDatabase(){
      Student student = new Student();
      student.setName("Robert");
      student.setRollNo("10");
      return student;
   }
}
```

[remark]:<slide>(new)
## Visitor
Tento vzor umožňuje pro skupinu třídy definovat nové operace, anižby bylo nutné jakkoliv měnit kód těchto tříd.

[remark]:<slide>(wait)
![](media/Visitor1.svg)

![](media/Visitor2.svg)

[remark]:<slide>(new)
### Příklad implementace
Define an interface to represent element.

```java
public interface ComputerPart {
   public void accept(ComputerPartVisitor computerPartVisitor);
}
```

[remark]:<slide>(new)
Create concrete classes extending the above class.

```java
public class Keyboard implements ComputerPart {

   @Override
   public void accept(ComputerPartVisitor computerPartVisitor) {
      computerPartVisitor.visit(this);
   }
}
```

```java
public class Monitor implements ComputerPart {

   @Override
   public void accept(ComputerPartVisitor computerPartVisitor) {
      computerPartVisitor.visit(this);
   }
}
```

```java
public class Mouse implements ComputerPart {

   @Override
   public void accept(ComputerPartVisitor computerPartVisitor) {
      computerPartVisitor.visit(this);
   }
}
```

[remark]:<slide>(new)
```java
public class Computer implements ComputerPart {
	
   ComputerPart[] parts;

   public Computer(){
      parts = new ComputerPart[] {new Mouse(), new Keyboard(), new Monitor()};		
   } 


   @Override
   public void accept(ComputerPartVisitor computerPartVisitor) {
      for (int i = 0; i < parts.length; i++) {
         parts[i].accept(computerPartVisitor);
      }
      computerPartVisitor.visit(this);
   }
}
```

[remark]:<slide>(new)
Define an interface to represent visitor.

```java
public interface ComputerPartVisitor {
	public void visit(Computer computer);
	public void visit(Mouse mouse);
	public void visit(Keyboard keyboard);
	public void visit(Monitor monitor);
}
```

[remark]:<slide>(new)
Create concrete visitor implementing the above class.

```java
public class ComputerPartDisplayVisitor implements ComputerPartVisitor {

   @Override
   public void visit(Computer computer) {
      System.out.println("Displaying Computer.");
   }

   @Override
   public void visit(Mouse mouse) {
      System.out.println("Displaying Mouse.");
   }

   @Override
   public void visit(Keyboard keyboard) {
      System.out.println("Displaying Keyboard.");
   }

   @Override
   public void visit(Monitor monitor) {
      System.out.println("Displaying Monitor.");
   }
}
```

[remark]:<slide>(new)
Use the ComputerPartDisplayVisitor to display parts of Computer.

```java
public class VisitorPatternDemo {
   public static void main(String[] args) {

      ComputerPart computer = new Computer();
      computer.accept(new ComputerPartDisplayVisitor());
   }
}
```

[remark]:<slide>(new)
## Memento
Zabezpečuje uchování stavů objektů, aby bylo v případě potřeby možné uvést objekt do původného stavu. 

Přitom zabezpečuje, aby uchování stavu nenarušilo zapouzdření objektu.

[remark]:<slide>(wait)
![](media/Memento.svg)

[remark]:<slide>(new)
### Příklad implementace

Create Memento class.

```java
public class Memento {
   private String state;

   public Memento(String state){
      this.state = state;
   }

   public String getState(){
      return state;
   }	
}
```

[remark]:<slide>(new)
Create Originator class

```java
public class Originator {
   private String state;

   public void setState(String state){
      this.state = state;
   }

   public String getState(){
      return state;
   }

   public Memento saveStateToMemento(){
      return new Memento(state);
   }

   public void getStateFromMemento(Memento memento){
      state = memento.getState();
   }
}
```

[remark]:<slide>(new)
Create CareTaker class

```java
import java.util.ArrayList;
import java.util.List;

public class CareTaker {
   private List<Memento> mementoList = new ArrayList<Memento>();

   public void add(Memento state){
      mementoList.add(state);
   }

   public Memento get(int index){
      return mementoList.get(index);
   }
}
```

[remark]:<slide>(new)
Use CareTaker and Originator objects.

```java
public class MementoPatternDemo {
   public static void main(String[] args) {
   
      Originator originator = new Originator();
      CareTaker careTaker = new CareTaker();
      
      originator.setState("State #1");
      originator.setState("State #2");
      careTaker.add(originator.saveStateToMemento());
      
      originator.setState("State #3");
      careTaker.add(originator.saveStateToMemento());
      
      originator.setState("State #4");
      System.out.println("Current State: " + originator.getState());		
      
      originator.getStateFromMemento(careTaker.get(0));
      System.out.println("First saved State: " + originator.getState());
      originator.getStateFromMemento(careTaker.get(1));
      System.out.println("Second saved State: " + originator.getState());
   }
}
```

[remark]:<slide>(new)
## Interpreter
Definuje reprezentaci gramatických pravidel jazyka pomocí systému tříd a současně definuje interpret tohoto jazyka pomocí metod instancí těchto tříd.

[remark]:<slide>(wait)
![](media/Interpreter.svg)

[remark]:<slide>(new)
### Příklad interpreteru
![](media/Interpreter_example.svg)

[remark]:<slide>(new)
### Příklad implementace

Create an expression interface.

```java
public interface Expression {
   public boolean interpret(String context);
}
```

[remark]:<slide>(wait)
Create concrete classes implementing the above interface.

```java
public class TerminalExpression implements Expression {
	
   private String data;

   public TerminalExpression(String data){
      this.data = data; 
   }

   @Override
   public boolean interpret(String context) {
   
      if(context.contains(data)){
         return true;
      }
      return false;
   }
}
```

[remark]:<slide>(new)
```java
public class OrExpression implements Expression {
	 
   private Expression expr1 = null;
   private Expression expr2 = null;

   public OrExpression(Expression expr1, Expression expr2) { 
      this.expr1 = expr1;
      this.expr2 = expr2;
   }

   @Override
   public boolean interpret(String context) {		
      return expr1.interpret(context) || expr2.interpret(context);
   }
}
```

```java
public class AndExpression implements Expression {
	 
   private Expression expr1 = null;
   private Expression expr2 = null;

   public AndExpression(Expression expr1, Expression expr2) { 
      this.expr1 = expr1;
      this.expr2 = expr2;
   }

   @Override
   public boolean interpret(String context) {		
      return expr1.interpret(context) && expr2.interpret(context);
   }
}
```

[remark]:<slide>(new)
InterpreterPatternDemo uses Expression class to create rules and then parse them.

```java
public class InterpreterPatternDemo {

   //Rule: Robert and John are male
   public static Expression getMaleExpression(){
      Expression robert = new TerminalExpression("Robert");
      Expression john = new TerminalExpression("John");
      return new OrExpression(robert, john);		
   }

   //Rule: Julie is a married women
   public static Expression getMarriedWomanExpression(){
      Expression julie = new TerminalExpression("Julie");
      Expression married = new TerminalExpression("Married");
      return new AndExpression(julie, married);		
   }

   public static void main(String[] args) {
      Expression isMale = getMaleExpression();
      Expression isMarriedWoman = getMarriedWomanExpression();

      System.out.println("John is male? " + isMale.interpret("John"));
      System.out.println("Julie is a married women? " + isMarriedWoman.interpret("Married Julie"));
   }
}
```
