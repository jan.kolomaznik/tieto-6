package jm.java.annotation.lesson;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Documented
public @interface AuthorData {
    String value() default "";
    String name() default "";
}
