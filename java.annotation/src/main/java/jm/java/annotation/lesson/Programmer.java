package jm.java.annotation.lesson;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Documented
@AuthorData(value = "UNKNOWN")
public @interface Programmer {
    String value();
}
