= Lexikální elementy
:deckjs_theme: swiss
:backend: deckjs
:deckjs_transition: fade
:encoding: utf-8
:navigation:
:split:
:status:

== Typy lexikálních elementů
* Java rozlišuje několik druhů lexikálních elementů:
  - identifikátory,
  - komentáře,
  - konstanty,
  - klíčová slova,
  - operátory,
  - oddělovače a bílé znaky.

== Identifikátory
* Identifikátory v Javě mohou mít neomezenou délku.
* Malá a velká písmena se považují za rozdílné znaky.
* Každý identifikátor musí začínat písmenem nebo podtržítkem.
* Zbylá část víceznakového identifikátoru může navíc obsahovat číslice "0".."9" a znak "$".
* Pozn.: Pro zápis identifikátorů lze použít i některé národní znaky z kódování Unicode.

== Komentáře
* V Javě lze použít tři typy komentářů
  - Komentář začíná znaky `/* a ukončen je dvojicí */`.
  - Komentář je uvozen znaky `// a ukončen koncem řádku`.
  - Komentář, který je uvozen znaky `/** a ukončen znaky */`

=== Příklad

[source,java]
----
/* Toto je komentář
   na dvě řádky */

// Toto je komentář do konce řádky

/**
* Toto je dokumentační komentář - obsahuje speciální značku:
* @author Zdeněk Kotala
*/
----

== Klíčová slova
* Java má vyhrazeno několik klíčových slov, která nesmí být použita pro názvy identifikátorů.

`assert`, `abstract`, `default`, `synchronized`, `boolean`, `do`, `if`, `this`, `break`, `double`, `implements`, `package`, `throw`, `byte`, `else`, `import`, `private`, `throws`, `extends`, `protected`, `transient`, `case`, `false`, `instanceof`, `public`, `true`, `final`, `int`, `try`, `catch`, `finally`, `interface`, `return`, `char`, `float`, `long`, `short`, `void`, `class`, `for`, `native`, `static`, `volatile`, `new`, `super`, `while`, `continue`, `null`, `switch`

== Konstanty
* Javě existuje několik typů konstant

=== Celočíselné konstanty
* **decimálním** - konstanta se skládá z číslic `0-9` a nesmí začínat číslicí `0` a může obsahovat znak `_`,
* **hexadecimálním** - konstanta začíná sekvencí `0x` nebo `0X` a kromě číslic `0-9` může obsahovat znaky `abcdefABCDEF`,
* **oktalovém** - konstanta začíná číslicí `0` a dále smí obsahovat jen číslice `0-7`.
* Všechny celočíselné konstanty jsou implicitně typu `int`.
  - Pro změnu typu na long je třeba za konstantu připojit znak `l` nebo `L`

<<<

=== Řetězcové konstanty
* Řetězcová konstanta se skládá z libovolného počtu znaků Unicode uzavřených v uvozovkách.
* Některé speciální znaky lze zapisovat pomocí escape sekvencí
  - \t	tabulátor
  - \n	nový řádek (LF)
  - \"	uvozovky
  - \'	apostrof
  - \\	zpětné lomítko

<<<

=== Racionální konstanty
* Racionální konstanta musí obsahovat alespoň jednu číslici a lze ji zapsat ve tvaru:
  - **Exponenciálním** - konstanta musí obsahovat znak e nebo E, za kterým následuje exponent.
  - **Semilogaritmickém** - konstanta vždy musí obsahovat desetinou tečku.
* Racionální konstanta je automaticky považována za typ `double`.
  - Pro změnu celočíselné na `double` se připojuje znak `d` nebo `D`.
  - Pro změnu na typ `float` se  připojení znak `f` nebo `F`.

<<<

=== Logické konstanty
* Pro typ boolean jsou definovány dvě konstanty:
  - `true` - reprezentující logickou 1,
  - `false` - reprezentující logickou 0.

<<<

=== Konstanty typu třída/objekt
* Jako konstanty je rovněž použít třídy a objekty
* speciální hodnota `null` se používá pro prázdný objekt.