package jm.java.collections.exercise;


import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by xkoloma1 on 02.11.2016.
 */
public class Cviceni {

    private final int kapacita;

    private final Set<Student> students;

    private Predmet predmet;

    public Cviceni(int kapacita) {
        this(kapacita, null);
    }

    public Cviceni(int kapacita, Predmet predmet) {
        this.kapacita = kapacita;
        this.predmet = predmet;
        this.students = new HashSet<>();
    }

    public int getKapacita() {
        return kapacita;
    }

    public boolean prihlasit(Student student) {
        // Task 1 test
        if (students.size() == kapacita) {
            return false;
        }
        // Task 4 test
        if (predmet != null && !predmet.getStudentyBezCviceni().contains(student)) {
            return false;
        }
        return students.add(student);
    }

    public Set<Student> getStudents() {
        return Collections.unmodifiableSet(students);
    }

    @Override
    public String toString() {
        return "Cviceni{" +
                "kapacita=" + kapacita +
                ", students=" + students +
                ", predmet=" + predmet +
                '}';
    }
}
