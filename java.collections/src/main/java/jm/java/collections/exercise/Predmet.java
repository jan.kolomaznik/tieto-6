package jm.java.collections.exercise;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by xkoloma1 on 02.11.2016.
 */
public class Predmet {

    private final String kod;

    private List<Cviceni> cvicenis;
    private SortedSet students;
   
    public Predmet(String kod) {
        this.kod = kod;
        this.cvicenis = new ArrayList<>();
        this.students = new TreeSet();
    }

    public Cviceni zalozCviceni(int kapacity) {
        Cviceni newCviceni = new Cviceni(kapacity, this);
        cvicenis.add(newCviceni);
        return newCviceni;
    }

    public Cviceni getCviceni(int index) {
        return cvicenis.get(index);
    }

    public List<Cviceni> getCvicenis() {
        return Collections.unmodifiableList(cvicenis);
    }

    public boolean zapisDoPredmetu(Student student) {
        return students.add(student);
    }

    public SortedSet getStudents() {
        return Collections.unmodifiableSortedSet(students);
    }

    public SortedSet getStudentyBezCviceni() {
        SortedSet result = new TreeSet(students); // copy set;
        result.removeAll(cvicenis.stream()
                .flatMap(c -> c.getStudents().stream())
                .collect(Collectors.toSet()));
        return result;
    }
}
