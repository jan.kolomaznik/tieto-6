package jm.java.collections.exercise;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

public class Registrace {

    private Predmet predmet;

    private Queue<Student> studentQueue;

    public Registrace(Predmet predmet) {
        this.predmet = predmet;
        this.studentQueue = new LinkedList<>();
    }

    public void zaregistruj(Student student) {
        if (studentQueue.contains(student)) {
            return;
        }

        studentQueue.add(student);
    }

    public Queue<Student> getStudents() {
        return studentQueue;
    }

    public int hromadnyZapis() {
        int max = predmet.getCvicenis().stream()
                .mapToInt(Cviceni::getKapacita)
                .sum();
        studentQueue.stream()
                .limit(max)
                .forEach(predmet::zapisDoPredmetu);
        return max - studentQueue.size();
    }
}
