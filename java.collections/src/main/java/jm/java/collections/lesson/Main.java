package jm.java.collections.lesson;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Main extends Object {

    static class Data implements Comparable<Data> {
        private final int number;
        private final String str;

        public Data(int number, String str) {
            this.number = number;
            this.str = str;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Data data = (Data) o;
            return number == data.number &&
                    Objects.equals(str, data.str);
        }

        @Override
        public int hashCode() {
            return Objects.hash(number, str);
        }

        @Override
        public String toString() {
            return "Data{" +
                    "number=" + number +
                    ", str='" + str + '\'' +
                    '}';
        }

        @Override
        public int compareTo(Data o) {
            if (number != o.number)
                return number - o.number;

            return str.compareTo(o.str);
        }
    }

    public static void main(String[] args) {
        Set<Data> set = new HashSet<>();
        Data data = new Data(1, "one");
        set.add(data);
        System.out.println(set);
        set.add(new Data(2, "one"));
        System.out.println(set);
        //data.number = 2;
        System.out.println(set);
        set.add(new Data(2, "one"));
        System.out.println(set);

    }

}
