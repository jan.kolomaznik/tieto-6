[remark]: <> (class: center, middle)

Paralelní programování
======================

## Vytváření procesů *(java)*
* Procesy reprezentuje třída `java.lang.Process`.
* Vytvořit ji lze dvěma způsoby:

```java
Runtime.getRuntime().exec(command);
```

```java
ProcessBuilder pb = new ProcessBuilder(
         "myCommand", "myArg1", "myArg2");
Map<String, String> env = pb.environment();
env.put("VAR1", "myValue");
env.remove("OTHERVAR");
pb.directory(new File("myDir"));
File log = new File("log");
pb.redirectErrorStream(true);
pb.redirectOutput(Redirect.appendTo(log));
Process p = pb.start();
p.waitFor();
```

---
## Komunikace s procesy
* Probíhá pomocí prostředků poskytovaných operačním systém
* Nejsnadnější způsob je pomocí `stdin`/`stdout`

**Zaslání zprávy procesu `p`**

```java
OutputStream stdin = p.getOutputStream();
stdin.write(message);
stdin.flush();
```

**Čtení zprávy od procesu `p`**

```java
InputStream stdout = p.getInputStream();
message = stdin.read();
```

---
## Vláknově (ne)bezpečné proměnné
* Lokální proměnná jsou vždy.

```java
public void someMethod(){
  long threadSafeInt = 0;
  threadSafeInt++;
}
```


