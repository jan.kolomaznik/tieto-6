package jm.java.concurrency;

import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable {

    protected BlockingQueue queue = null;

    public Consumer(BlockingQueue queue) {
        this.queue = queue;
    }

    public void run() {
        try {
            while (true) {
                System.out.println(queue.take());
                ThreadUnit.activeWork(1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

