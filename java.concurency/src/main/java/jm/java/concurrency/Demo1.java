package jm.java.concurrency;

import java.util.concurrent.Semaphore;

public class Demo1 {

    public static void main(String[] args) {
        ThreadUnit.sleep(3000);
        Semaphore semaphore = new Semaphore(2);
        Runnable threadCode = () -> {
            while (true) {
                //try {
                    System.out.println(Thread.currentThread().getName() + ": loop");
                    //semaphore.acquire();
                    synchronized (Demo1.class) {
                        System.out.println(Thread.currentThread().getName() + ": work");
                        ThreadUnit.activeWork(3000);
                        //semaphore.release();
                    }
                    System.out.println(Thread.currentThread().getName() + ": sleep");
                    ThreadUnit.sleep(1000);
                //} //catch (InterruptedException e) {
                  //  e.printStackTrace();
                //}
            }
        };

        new Thread(threadCode).start();
        new Thread(threadCode).start();
        new Thread(threadCode).start();

    }
}
