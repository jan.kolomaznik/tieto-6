package jm.java.concurrency;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Main {

    public static void main(String[] args) {

        ReadWriteLock lock = new ReentrantReadWriteLock();
        Runnable writer = () -> {
            while (true) {
                ThreadUnit.sleep(7000);
                lock.writeLock().lock();
                ThreadUnit.activeWork(500);
                lock.writeLock().unlock();
            }
        };

        Runnable reader = () -> {
            while (true) {
                ThreadUnit.sleep(600);
                lock.readLock().lock();
                ThreadUnit.activeWork(800);
                lock.readLock().unlock();
            }
        };

        new Thread(writer).start();
        new Thread(reader).start();
        new Thread(reader).start();
        new Thread(reader).start();
        new Thread(reader).start();
        new Thread(reader).start();
        new Thread(reader).start();
        new Thread(reader).start();

    }
}
