package jm.java.concurrency;

import java.util.concurrent.BlockingQueue;

public class Producer implements Runnable {

    protected BlockingQueue queue = null;

    public Producer(BlockingQueue queue) {
        this.queue = queue;
    }

    public void run() {
        int number = 0;
        try {
            while (true) {
                ThreadUnit.activeWork(2000);
                queue.put(number++);
                ThreadUnit.sleep(500);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
