package jm.java.concurrency;

public class ThreadUnit {

    public static void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void activeWork(long time) {
        long finishTime = System.currentTimeMillis() + time;
        while (finishTime > System.currentTimeMillis()) {
            // Do nothing
        }
    }
}
