package cz.mendelu.pjj.robot;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

public class Coordinate {

    private static AtomicLong counter = new AtomicLong();

    private static Map<String, Coordinate> cache = new HashMap<>();

    public static Coordinate coordinate(int x, int y) {
        String key = x + ":" + y;
        if (!cache.containsKey(key)) {
            if (counter.incrementAndGet() % 10 == 0) {
                System.out.println(counter);
            }
            cache.put(key, new Coordinate(x, y));
        }

        return cache.get(key);
    }

    public final int x;
    public final int y;

    private Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinate that = (Coordinate) o;
        return x == that.x &&
                y == that.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "Coordinate{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
