package cz.mendelu.pjj.robot;

import static cz.mendelu.pjj.robot.Coordinate.coordinate;

/**
 * Created by Honza on 09.11.2016.
 */
public enum Direction {

    NORTH(0, -1),
    EAST (1, 0),
    SOUTH(0, 1),
    WEST (-1, 0);

    private static final int LENGTH = Direction.values().length;
    private static final Direction[] VALUES = Direction.values();

    private final int dx;
    private final int dy;

    Direction(int dx, int dy) {
        this.dx = dx;
        this.dy = dy;
    }

    public Coordinate moveStep(Coordinate from) {
        int x = from.x + dx;
        int y = from.y + dy;
        return coordinate(x, y);
    }

    public Direction onRight() {
        int ordinal = this.ordinal();
        int newOrdinal = (ordinal + 1) % LENGTH;
        return VALUES[newOrdinal];
    }

    public Direction onLeft() {
        int ordinal = this.ordinal();
        int newOrdinal = (ordinal + LENGTH - 1) % LENGTH;
        return VALUES[newOrdinal];
    }


}
