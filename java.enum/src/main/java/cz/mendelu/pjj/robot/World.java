package cz.mendelu.pjj.robot;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Honza on 08.11.2016.
 */
public class World {

    private final int width;
    private final int height;
    private Map<Coordinate, Object> map;

    public World(int width, int height) {
        this.width = width;
        this.height = height;
        this.map = new HashMap<>();
    }

    public Object getTreasureAt(int x, int y){
        // FIXME Upravit v samostatné práci
        return map.get(Coordinate.coordinate(x, y));
    }

    public void removeTreasureAt(int x, int y){
        // FIXME Upravit v samostatné práci
        map.remove(Coordinate.coordinate(x, y));
    }

    public Object addTreasureAt(Object object, int x, int y){
        map.put(Coordinate.coordinate(x, y), object);
        return object;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

}
