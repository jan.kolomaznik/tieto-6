package jm.java.functions.lesson;

public class LocalPoint implements Point {

    @Override
    public double getX() {
        return 1;
    }

    @Override
    public double getY() {
        return 2;
    }
}
