package jm.java.functions.lesson;

public interface Point {

    double getX();
    double getY();

    default double distance(Point o) {
        return Math.abs(this.getX() - o.getX()) + Math.abs(this.getY() - o.getY());
    }
}
