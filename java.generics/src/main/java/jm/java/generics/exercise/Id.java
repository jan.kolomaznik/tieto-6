package jm.java.generics.exercise;

public class Id<V> {

    private final long value;

    public Id(long value) {
        this.value = value;
    }

    public long getValue() {
        return value;
    }
}
