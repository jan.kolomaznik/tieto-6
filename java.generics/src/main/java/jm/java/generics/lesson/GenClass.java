package jm.java.generics.lesson;

public class GenClass<T> {

    private Class<T> tClass;

    public GenClass(Class<T> tClass) {
        this.tClass = tClass;
    }

    public String TTypy() {
        return tClass.getCanonicalName();
    }
}
