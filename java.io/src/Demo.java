import java.io.*;

public class Demo implements AutoCloseable {

    public static void main(String[] args) throws IOException {
        try (Demo demo = new Demo()) {
            System.out.println("Used");
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Finish");
        File tmp = File.createTempFile("java", ".md");
        tmp.deleteOnExit();
    }

    @Override
    public void close() throws Exception {
        System.out.println("Close");
    }
}
