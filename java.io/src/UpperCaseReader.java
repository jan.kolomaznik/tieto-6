import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

public class UpperCaseReader extends Reader {

    public static void main(String[] args) {
        StringReader deg = new StringReader("ahoj");

        try (BufferedReader in = new BufferedReader(new UpperCaseReader(deg))){
            System.out.println(in.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Reader delegate;

    public UpperCaseReader(Reader delegate) {
        this.delegate = delegate;
    }

    @Override
    public int read(char[] cbuf, int off, int len) throws IOException {
        int result = delegate.read(cbuf, off, len);
        for (int i = off; i < off + result; i++) {
            cbuf[i] = Character.toUpperCase(cbuf[i]);
        }
        return result;
    }

    @Override
    public void close() throws IOException {
        delegate.close();
    }
}
