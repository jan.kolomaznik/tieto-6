package jm.java.streams.lesson;

import java.util.Date;
import java.util.function.Function;
import java.util.function.Supplier;

public class DateExample {

    public static void main(String[] args) throws InterruptedException {
        //Date date = new Date();
        Supplier<Date> noParamDate = Date::new;
        Function<Long, Date> longParamDate = Date::new;

        System.out.println(noParamDate.get());
        System.out.println(longParamDate.apply(0l));
    }
}
