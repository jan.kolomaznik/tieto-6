package jm.java.streams.lesson;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.UnaryOperator;

public class LambdaForm {
    private JTextField inputTest;
    private JButton button1;
    private JButton button2;
    private JLabel resultLabel;
    private JPanel root;

    public LambdaForm() {
        button1.addActionListener(e -> resultLabel.setText(op1.apply(inputTest.getText())));
        button2.addActionListener(e -> resultLabel.setText(op2.apply(inputTest.getText())));
    }

    private UnaryOperator<String> replaceX = str -> str.replaceAll("\\d", "X");
    private UnaryOperator<String> countX = str -> Boolean.toString(str.contains("X"));
    private Function<String, String> op1 = replaceX.andThen(countX);
    private Function<String, String> op2 = replaceX.compose(countX);

    public static void main(String[] args) throws InvocationTargetException, InterruptedException {
        SwingUtilities.invokeLater(() -> {
                LambdaForm lambdaForm = new LambdaForm();
                JFrame jframe = new JFrame("Test");
                jframe.setContentPane(lambdaForm.root);
                jframe.pack();
                jframe.setVisible(true);
            }
        );
    }
}
