[remark]:<class>(center, middle)
# Datové proudy
## Streams

[remark]:<slide>(new)
## Úvod
Při práci s klasickými poli a kolekcemi se používají především externí (sekvenční) iterátory

Program požádá kolekci o iterátor, který pak v cyklu žádá o další instanci, s níž provede požadovanou operaci.

Proud se chová jako objekt využívající interní (dávkové) iterátory provádějící s iterovanými objekty operace definované lambda-výrazy.

Program definuje, jaká data se do proudu zařadí, resp. jak proud ona data získá.

Nezávisle na tom se definuje, jak se těmito daty bude pracovat.

[remark]:<slide>(new)
### Analogie: výrobní linka
Na počátku je vstup
* V případě výrobního pásu je to sklad dílů, v případě proudu to bude zdroj dat – většinou nějaký zdrojový kontejner.
* Zdrojem ale může být i generátor, který vytváří data na požádání

Pracoviště
* Podél výrobního pásu jsou připravená pracoviště, na nichž zaškolení dělníci provádějí jednotlivé operace.
* V případě proudu nahradíme dělníky metodami (přesněji lambda výrazy), které s příchozím objektem provedou požadovanou operaci
* Zpracovaný objekt pokračuje po proudu k dalšímu pracovišti

Výsledky
* Na konci výrobního pásu „vypadne“ hotový výrobek.
* Na konci proudu obdržíme požadovaný výsledek.

[remark]:<slide>(new)
### Odchylky od kolekcí a polí
Neblokují si žádnou paměť pro zpracovávaná data; data odněkud „přitékají“, „pracoviště“ je zpracuje a pošle dál.

Ve většině případů neovlivňují zdrojová data.

Naplánované operace se chovají obdobně jako dělníci u pásu: nehrnou se do skladu (kontejneru), aby v něm data zpracovaly, ale počkají si, až k nim zpracovávaný objekt po proudu „přiteče“ a pak se o něj postarají.

Nezajímají se o to, zda je vstup dat konečný (klasický kontejner), nebo nekonečný.

Z programu zmizí pomocný kód, který měl na starosti řízení iterací, a zpřehlední se tak popis akcí, které se mají se zpracovávanými objekty udělat.

Objeví-li se v budoucnu nějaké propracovanější techniky paralelizace prováděných činností, není třeba upravovat část programu řešící aplikační logiku, ale stačí pouze vylepšit knihovnu proudů a tím se automaticky zefektivní zpracování programů, které tyto proudy využívají.

[remark]:<slide>(new)
### Imperativní příklad
```java
private static Set<String> findNamesByAge(Person[] persons, int age) {
    
    Set<String> result = new HashSet<>(persons.length);
    
    for (int i = 0; i < persons.length; i++) {
        Person p = persons[i];
        if (p.getAge() < age) {
            result.add(p.getName());
        }
    }
    
    return result;
}
```

```java
Set<String> names = findNamesByAge(persons, 18);
if (names.contains("John Doe")) {
    System.out.println("John Doe exists!");
}
```

[remark]:<slide>(new)
### Imperativní příklad: krok 1
```java
private static Set<String> findNames(
        Iterable<? extends Person> persons, 
        Predicate<? super Person> condition) {
    
    Set<String> result = new HashSet<>();
    
    for (Person person : persons){
        if (condition.test(person)) {
            result.add(p.getName());
        }
    }
    
    return result;
}

```

```java
Predicate<Person> condition = p -> (p.getAge() < 18) 
        && "John Doe".equals(p.getName());

if (!findNames(persons, condition).isEmpty()) {
    System.out.println("John Doe exists!");
}
```

[remark]:<slide>(new)
### Imperativní příklad: krok 2
```java
Collection<Person> persons = …

Predicate<Person> condition = p -> (p.getAge() < 18) 
        && "John Doe".equals(p.getName());

if (persons.stream().anyMatch(condition)) {
    System.out.println("John Doe exists!");
}
```

[remark]:<slide>(new)
### Více lambda příklad
```java
List<String> cislaSlovne = Arrays.asList("200", "400", "100", "300");
Integer[] cisla = cislaSlovne
                .stream()
                .map(Integer::parseInt)
                .filter(num -> num < 300)
                .toArray(Integer[]::new);

Arrays.stream(cisla).forEach(System.out::println);
```
Co vypíše?

[remark]:<slide>(new)
## Možnosti získání proudu
Z **kolekcí** prostřednictvím metod:
* `stream()`
* `parallelStream()`

Z **polí** prostřednictvím metody
* `Arrays.stream(Object[])`

Z **továrních** metod třídy `Stream`:
* `Stream of(Object[])`
* `IntStream range(int, int)`
* `Stream iterate(Object, UnaryOperator)`

**Řádky souboru** lze získat metodou `BufferedReader.lines()`

Proud **náhodných čísel** lze získat metodou `Random.ints()`

Proud položek v **ZIP-souboru** lze získat metodou `JarFile.stream()`


[remark]:<slide>(new)
## Práce s obsahem proudu
Obsah proudu můžeme během práce **filtrovat** použitím metody:
* `Stream<T> filter(Predicate<? super T> predicate)`

Proudy můžeme také **řadit**:
* `Stream<T> sorted()`
* `Stream<T> sorted(Comparator<? super T> comparator)`

Proudy můžeme také **odstranit duplicity**:
* `Stream<T> distinct()`

[remark]:<slide>(new)
Prvky proudu můžeme **konvertovat** na jiný typ:
* `<R> Stream<R> map(Function<? super T,? extends R> mapper)`
* `DoubleStream mapToDouble(ToDoubleFunction<? super T> mapper)`
* `IntStream mapToInt (ToIntFunction <? super T> mapper)`
* `LongStream mapToLong (ToLongFunction <? super T> mapper)`

**Měnit dimenzi** streamu:
* `<R> Stream<R> flatMap(Function<? super T,? extends Stream<? extends R>> mapper)`
* `DoubleStream flatMapToDouble(Function<? super T,? extends DoubleStream> mapper)`
* `IntStream flatMapToInt(Function<? super T,? extends IntStream> mapper)`
* `LongStream flatMapToLong(Function<? super T,? extends LongStream> mapper)`

**Ohraničit** stream:
* `Stream<T> limit(long maxSize)`
* `Stream<T> skip(long n)`

[remark]:<slide>(new)
A další operátoru jako:
* `forEach`, `reduce, `collect`
* `anyMatch`, `allMatch`, `noneMatch`
* `findFirst`, `findAny`
* `min`, `max`, `count`

[remark]:<slide>(wait)
#### Praktické příklady
[Streams.java](src/main/java/jm/java/streams/lesson/Streams.java)

[Idioms.java](src/main/java/jm/java/streams/lesson/Idioms.java)

[Parallelism.java](src/main/java/jm/java/streams/lesson/Parallelism.java)

[remark]:<slide>(new)
[remark]:<class>(center, middle)
# Cvičení

Projděte si ukoly v souborech:

[CollectionBasics.java](src/main/java/jm/java/lambda/exercise/CollectionBasics.java)

[TimePoint.java](src/main/java/jm/java/lambda/exercise/TimePoint.java)

