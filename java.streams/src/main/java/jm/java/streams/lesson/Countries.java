package jm.java.streams.lesson;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Countries {

    public static class Country {
        public final long population;
        public final String name;
        public final Set<String> languages;

        public Country(Object o) {
            JSONObject jsonObject = (JSONObject) o;
            this.population = (long) jsonObject.get("population");
            this.name = (String) jsonObject.get("name");
            this.languages = (Set<String>) ((JSONArray)jsonObject.get("languages"))
                    .stream()
                    .map(lang -> ((JSONObject)lang).get(("name")))
                    .collect(Collectors.toSet());
        }

    }

    public static void main(String[] args) throws MalformedURLException {
        // prepar variables
        JSONParser jsonParser = new JSONParser();
        URL url = new URL("https://restcountries.eu/rest/v2/all");
        JSONArray jsonArray;

        // Read and parse json from rest endpoint.
        try (Reader in = new InputStreamReader(url.openStream())) {
            jsonArray = (JSONArray) jsonParser.parse(in);
        } catch (IOException | ParseException e) {
            e.printStackTrace();
            return;
        }

        // Print countries on console output.
        jsonArray.stream().forEach(System.out::println);

        // How many people live on the World?
        System.out.println(jsonArray.stream()
                .map(o -> new Country(o))
                .mapToLong(c -> ((Country) c).population)
                .sum());

        // Population of each country?
        System.out.println(jsonArray.stream()
                .map(Country::new)
                .map(c -> ((Country) c).name + ": " + ((Country) c).population)
                .collect(Collectors.toList()));

        System.out.println(jsonArray.stream()
                .map(Country::new)
                .collect(Collectors.groupingBy(
                        c -> ((Country)c).name,
                        Collectors.summingLong(c -> ((Country)c).population))));

        // Select countries where people speak english and sort them by populations
        List<Country> countries = (List<Country>) jsonArray.stream()
                .map(Country::new)
                .collect(Collectors.toList());

        countries.stream()
                .filter(c -> c.languages.contains("English"))
                .sorted(Comparator.comparingLong(c -> c.population))
                .map(c -> c.name + " (" + c.population + ")")
                .forEach(System.out::println);


    }


}
