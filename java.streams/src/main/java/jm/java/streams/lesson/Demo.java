package jm.java.streams.lesson;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Demo {

    public static void main(String[] args) {
        IntStream is = IntStream.range(1, 10);
        is.limit(3).forEach(System.out::println);

    }
}
