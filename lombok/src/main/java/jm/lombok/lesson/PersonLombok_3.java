package jm.lombok.lesson;

import lombok.*;

import java.time.LocalDate;

@Data
public class PersonLombok_3 {

    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;

    public static void main(String[] args) {
        PersonLombok_3 person = new PersonLombok_3();
        person.setFirstName("Pepa");
        person.setLastName("Zeepa");
        System.out.println(person);
        System.out.println("Equals: " + person.equals(
                new PersonLombok_3()
        ));
    }
}

