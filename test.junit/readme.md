[remark]:<class>(center, middle)
# Testy: jUnit 5

[remark]:<slide>(new)
## Struktura projektu

Projekt pro Maven má automaticky vytvořené adresáře:
  - src/main/java pro třídy
  - src/test/java pro unit testy.

Mějme tedy například třídy v balíčku `jm.test.model` symbolizující aplikoační logiku aplikace, kterou chceme otestovat.

## JUnit 5 (Osnova)
Annotations
Test Classes and Methods
Display Names
Assertions
Assumptions
Disabling Tests
Conditional Test Execution
Test Execution Order
Nested Tests
Test Interfaces and Default Methods
Repeated Tests
Timeouts
