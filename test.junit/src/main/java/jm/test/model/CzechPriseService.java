package jm.test.model;

import org.junit.jupiter.api.BeforeEach;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class CzechPriseService implements PriceService {


    @Override
    public BigDecimal regularPrice(List<Order.Item> items) {
        return items.stream()
                .map(item -> item.itemPrise)
                .reduce(BigDecimal::add)
                .get();
    }

    private int delka(String a) {
        return a.length();
    }

    @Override
    public BigDecimal privilegePrice(User user, List<Order.Item> items) {
        return null;
    }
}
