package jm.test.model;

import java.time.LocalDate;

/**
 * Rozhranní pro práci se skladem zboži
 */
public interface WarehouseService {

    /**
     * Metoda pro získání aktuálního množstí produktu ve skladu
     */
    int getAmountOfStoredProduct(Product product);

    /**
     * Vrátí datum od kterého je bude zboří znovu naskladněno k distribuci
     * @param product
     * @return
     */
    LocalDate getNextProductStorageDay(Product product);
}
