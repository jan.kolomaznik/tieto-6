package jm.test.model

import spock.lang.Specification
import spock.lang.Unroll

import static org.junit.jupiter.api.Assertions.assertEquals

class CzechPriseServiceTest2 extends Specification {


    private CzechPriseService czechPriseService;

    public void setup() {
        czechPriseService = new CzechPriseService();
    }

    def "Test calcutate regularPrise from two items."() {
        given:
        List<Order.Item> items = Arrays.asList(
                new Order.Item(null, regularPrvoidicenull, BigDecimal.ONE),
                new Order.Item(null, null, BigDecimal.TEN)
        );

        when:
        BigDecimal prise = czechPriseService.regularPrice(null, items);

        then:
        11 == prise;
    }

    def "Test calcutate regularPrise  items."() {
        given:
        List<Order.Item> items = Arrays.asList(
                new Order.Item(null, regularPrvoidicenull, BigDecimal.ONE),
                new Order.Item(null, null, BigDecimal.TEN)
        );

        when:
        BigDecimal prise = czechPriseService.regularPrice(null, items);

        then:
        11 == prise;

    }

    @Unroll
    def "maximum of {#a, #b} two numbers id #c"(int a, int b, int c) {
        expect:
        Math.max(a, b) == c

        where:
        a | b | c
        1 | 3 | 3
        7 | 4 | 7
        0 | 1 | 0
    }
}
