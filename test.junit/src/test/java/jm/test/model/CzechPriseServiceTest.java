package jm.test.model;

import org.junit.jupiter.api.*;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CzechPriseServiceTest {

    private CzechPriseService czechPriseService;

    @BeforeEach
    public void setup() {
        czechPriseService = new CzechPriseService();
    }

    @Test
    @DisplayName("Method regularPrise: Base scanario")
    void regularPrice() {
        // Prepare
        List<Order.Item> items = Arrays.asList(
                new Order.Item(null, null, BigDecimal.ONE),
                new Order.Item(null, null, BigDecimal.TEN)
        );

        // Using method
        BigDecimal prise = czechPriseService.regularPrice(items);

        // Asserts
        assertEquals(BigDecimal.valueOf(11), prise);

    }

    @Test
    void privilegePrice() {
    }
}