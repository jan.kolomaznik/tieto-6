package jm.test.model;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static org.junit.jupiter.api.condition.JRE.JAVA_8;

@Tag("model")
@TestMethodOrder(MethodOrderer.Alphanumeric.class)
class InMemoryWarehouseServiceTest {

    private InMemoryWarehouseService inMemoryWarehouseService;

    @BeforeEach
    @Timeout(value = 5, unit = TimeUnit.HOURS)
    public void initInMemoryWarehouseService() {
        this.inMemoryWarehouseService = new InMemoryWarehouseService();
    }

    /**
     * https://junit.org/junit5/docs/current/user-guide/#writing-tests-parameterized-test
     */
    //@ParameterizedTest
    //@ValueSource(strings = { "auto", "radar", "banan" })
    void addProduct(String name) {
        Product product = new Product(name, BigDecimal.TEN);
        inMemoryWarehouseService.addRecord(product, 10, null);

        assertEquals(10, inMemoryWarehouseService.getAmountOfStoredProduct(product));
    }

    @Test
    @DisplayName("Get amount of stored product.")
    void getAmountOfStoredProduct() {
        assumeTrue("DEV".equals(System.getenv("ENV")),
                () -> "Aborting test: not on developer workstation");

        Product product = new Product("Test 1", BigDecimal.TEN);
        inMemoryWarehouseService.addRecord(product, 10, null);

        assertEquals(10, inMemoryWarehouseService.getAmountOfStoredProduct(product));
    }

    @Test
    @Disabled("for demonstration purposes")
    void getAmountOfStoredProduct_nonExistsProduct() {
        Product product = new Product("Test 0", BigDecimal.TEN);

        assertEquals(0, inMemoryWarehouseService.getAmountOfStoredProduct(product));
    }

    @Test
    @EnabledOnJre(JAVA_8)
    //@RepeatedTest(3)
    void getNextProductStorageDay() {
        Product product_1 = new Product("Test 1", BigDecimal.TEN);
        Product product_2 = new Product("Test 2", BigDecimal.ZERO);

        inMemoryWarehouseService.addRecord(product_1, 10, LocalDate.of(2000,1,2));
        inMemoryWarehouseService.addRecord(product_2, 10, LocalDate.of(2003,4,5));

        assertAll("stored day",
                () -> assertEquals(LocalDate.of(2000,1,2), inMemoryWarehouseService.getNextProductStorageDay(product_1)),
                () -> assertEquals(LocalDate.of(2000,1,2), inMemoryWarehouseService.getNextProductStorageDay(product_2))
        );

    }
}